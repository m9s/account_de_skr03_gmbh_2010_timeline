# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'German Chart of Accounts SKR03 GmbH 2010 Timeline',
    'name_de_DE': 'Deutscher Kontenrahmen SKR03 GmbH 2010 Gültigkeitsdauer',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Financial and accounting module (only for Germany):
    - Provides chart of accounts SKR03 for GmbH (limited liability company)
      for the year 2010
    - Provides account structure like balance and income statement
    - Provides taxes, tax groups, tax rules
    - Provides tax codes for german tax report (UStVA)
    - Provides timeline features

    ''',
    'description_de_DE': '''Buchhaltungsmodul (für Deutschland):
    Stellt den Kontenrahmen SKR03 für eine GmbH für das Jahr 2010 zur Verfügung
    mit
    - Bilanzgegliederten Konten
    - Steuern, Steuergruppen und Steuerregeln
    - Steuerkennziffern für die Umsatzsteuervoranmeldung (UStVA)

    Stellt die Merkmale der Gültigkeitsdauermodule zur Verfügung
    ''',
    'depends': [
        'account_timeline_tax_de',
        'account_option_party_required',
    ],
    'xml': [
        'account_de_skr03_gmbh_2010_timeline.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
